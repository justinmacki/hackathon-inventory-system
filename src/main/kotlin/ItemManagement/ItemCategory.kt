package ItemManagement

/**
 *  This class shows the available categories of all the items.
 */

enum class ItemCategory (shortName: String, longName: String) {

    FROZEN ("FG","Frozen Goods"),
    DAIRY ("DG","Dairy Goods"),
    CANNED ("CG","Canned Goods"),
    ALCOHOL ("AG","Alcohol Goods"),
    CLEANING ("CP","Cleaning Products"),

}